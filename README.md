## Hi, I'm Kyle.
Currently a design lead at [sentry.io](https://sentry.io/welcome/) / [codecov.io](https://about.codecov.io/) and tinkering [withline.xyz](https://withline.xyz/). My path into design started with learning html/css/js, then building websites for fun, then freelancing, then UX work on complex problems. I’m endlessly fascinated by the intersection of humanities & technology, an introvert by nature, and a [rational optimist](http://www.rationaloptimist.com/) in my outlook. In my free time you’ll find me [reading](https://www.goodreads.com/user/show/100549438-kyle-mann), swimming, in nature, flânering, and forgetting why I came to the grocery store :thinking:.

### Communication
* **Email:** check once daily primarily to stay up-to-date with issue updates and scheduling
* **GitLab/GitHub:** please @kmann (GitLab) or @kylemann (GitHub) me directly when needing a response and/or to ensure I see it
* **Slack:** check daily and consider it asynchronous; it’s ideal for planning logistic needs or quick question-answers
* **Zoom:** ideal for conversations requiring back-and-forth in-depth dialogue
* **YouTube:** ideal for asynchronous reviews, demos, and discovery outcomes
* **Focus time:** blocked out 4 hours on calendar daily for focused work
* **Schedule a meeting:** send me an email at kyle@convexitydesign.com

### Values
I consider my process human-oriented, evolving, and adaptable. There is no one-size-fits-all approach, rather common considerations including: Who are we solving problems for? Who am I working with? How do we best communicate? What inspires them? What inspires us? What inspires our user? How can we evolve our practice? These questions form the foundation for my approach. Though, deeper than my process are my values:
* Candor and respect are not mutually exclusive; both must be practiced simultaneously
* Diversity in all forms is essential
* Always assume best intentions in others: listen, learn, and be humble
* Teams must move together cohesively with shared purpose and common goals
* Known unknown: know that you don’t know, always stay a student
* Build on strengths and intrinsic motivations 
* Trust is a precursor to great work

### Influences
* [Edward Tufte](https://www.edwardtufte.com/tufte/) taught me the essentials in information design and seeing beauty in the most simple things. Key lessons: graphical integrity, keep items in space not time (same vs different pages), and “clutter and confusion are failures of design, not attributes of information”.

* [Daniel Kahneman](https://en.wikipedia.org/wiki/Daniel_Kahneman) and collaborator Amos Tversky work translates to foundational user experience lessons in human perception. Key concepts: system 1 and 2, anchoring, availability, loss aversion, hindsight bias, and WYSIATI.

* I’ve been privileged with lifelong collaborators, clients, friends, and colleagues that continually teach me what I love, new perspectives, and how to improve. Learning from my peers, through collaborative experience, has been the single best learning tool.

* Too many to list, but here are [few more](https://kyle-mann.com/about.html).
