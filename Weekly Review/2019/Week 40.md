# September 30th - October 4th
## Focus
* Reach out to product to clarify objectives on this issue https://gitlab.com/gitlab-org/gitlab/issues/14059
* Update configuration screen issue https://gitlab.com/gitlab-org/gitlab/issues/13638, based on user test feedback
* Update discovery compliance policy: https://gitlab.com/gitlab-org/gitlab/issues/12941
* Review and identify UX needs with https://gitlab.com/gitlab-org/gitlab/issues/11119: Distinguish two vulnerabilities impacting the same dependency but with different versions


#### Progress
* UX MVC and improvements for consideration on policy discovery: https://gitlab.com/gitlab-org/gitlab/issues/12941
* Scheduled shadow issue to work on with Camellia: https://gitlab.com/gitlab-org/ux-research/issues/359, https://gitlab.com/gitlab-org/gitlab/issues/12941, and  https://gitlab.com/gitlab-org/gitlab/issues/13992. 
 
#### Opportunities
* Need to qualify issues more, prior to being planned on a milestone. There are cases of issues that are planned, that include limited-to-none details or expectations. By time updates and/or clarifications are done the milestone is almost over.
* Discoveries may be improved with the following: qualified expectations/goals/problems to review outlined prior to planning, cross-functional discovery kickoff, and a designated lead (whether PM, UX, or ENG)  

 
### Meetings
*Doesn't include recurring company/stage group meetings*

* UX buddy syncs: design system review, issue review, issue kickoff, weekly 1:1
* 3 UX research sessions (note taking)
* 2 syncs with Camellia and Tali to review research findings/analysis and design actions
* Sync with Fabien about: `https://gitlab.com/gitlab-org/gitlab/issues/11119`

### 12.4 Review
[View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)