# December 2nd - 6th
...

## Focus
* Finalize prototype and prep work for user study and recruiting
* User studies and post-review team discussions (6 participants)
* Consider and draft an issues about Secure settings growth strategy
* Review epic status and consider closing, as most issues have been or will be shipped: https://gitlab.com/groups/gitlab-org/-/epics/1618
* Work on any missed ToDos [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing), migrate slipped items to 12.6 document

#### Progress
* Concluded cross-feature user study. Included feedback about: auto-remediation, settings information architecture, banner notification on project dashboard, security gates MR UX, and new vulns object page
* Discussed and identified areas of focus amongst Secure team

#### Opportunities
* Prototype existing MVC proposals that have not been built (and are scheduled) and include in upcoming user tests
* Discuss info-architecture findings and align on a strategy
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* Discussion about security gates w/Camellia
* Review 1st class objects for user test prep w/ Andy
* Tali/Camellia final prep for user test
* Coffee chat w/ Daniel Mora
* UX buddy weekly w/ Camellia
* 6 usability tests
* Review license classifications in Front-end needing change w/ Fernando
* UX planning sync kickoff
* Onboarding sync w/ Annabel
* Extended pair sync with Dimitrie to brainstrom workflow process
* Intro with Derek F. 


