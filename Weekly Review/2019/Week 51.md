# December 16th - 20th

## Focus
* Ideate on auto-remediate MR  author alternative
* Prepare walkthrough document and schedule live sync for new product members
* Review 12.7/8 for planning review and recommendations
* Work on any missed ToDos [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing), migrate slipped items to 12.6/7 document as needed
 
#### Progress
* Worked on license compliance foundation recap, will continue to add video walkthrough after holidays [View document](https://docs.google.com/document/d/1XbtJH8guy_vTY-3Lgq6lFK_9KTdop-8D4YHnyQqZamg/edit?usp=sharing) 
* Working on improvement to auto-remediation MVC https://gitlab.com/gitlab-org/gitlab/issues/36500#note_260858531
, per insight: https://gitlab.com/gitlab-org/uxr_insights/issues/880* Identified direction and short-term strategy for settings https://gitlab.com/gitlab-org/gitlab/issues/39209
Revaluate auto-remediation author 
* Prepped for upcoming Secure showcase (Auto-remediation) - [View slides](https://docs.google.com/presentation/d/1cuFtG3OOweB6ROzX0aV8grSIVCUE7Cj2s4OfRfin36o/edit?usp=sharing) 
* UI/UX baseline walkthrough w/ product [Walkthrough doc](https://docs.google.com/document/d/1d_VkrdyRK8Nxlw7W_AKENE4S8J-HJwZXSABTimT9ep4/edit?usp=sharing)

#### Opportunities
* Epics to help EM/PM prioritization and planning; examples: 1) Approver rules/security gates: https://gitlab.com/groups/gitlab-org/-/epics/2319, 2) License Compliance (projects): https://gitlab.com/groups/gitlab-org/-/epics/2328, 3) Auto-remediation: https://gitlab.com/groups/gitlab-org/-/epics/2238. (Include related research validation issues)
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* Product development workflow review w/ Olivier, Lukas, and Nicole
* Compliance overlap review Manage/Secure, w/ Daniel M.
* UX buddy sync w/ Camellia
* Secure/Defend Product 3-year review
* Pre-milestone planning session
* Pair design review w/Dimitrie
* Sync w/ Olivier
* Skip level Secure/Defend meeting
* Secure buddy w/ Annabel
 
### Milestone Review
12.7/8 under review
