# October 14th-18th
Monday OOO per holiday
## Focus
* Finalize implementation plan update deliverables needed, per discovery: https://gitlab.com/gitlab-org/gitlab/issues/12941
* Finalize implementation plan and update deliverables needed, per discovery: https://gitlab.com/gitlab-org/gitlab/issues/13646
* Review, groom, add issues for 12.5 needs
#### Progress
* Updated issue to reflect outcomes: https://gitlab.com/gitlab-org/gitlab/issues/12941#discovery-outcomes
* Finalizing configuration screeen implementation issue for UX Ready: https://gitlab.com/gitlab-org/gitlab/issues/13638#note_231878495
* Surfaced some great feedback that clarified: https://gitlab.com/gitlab-org/gitlab/issues/14059#note_227616774
 
#### Opportunities
* Improve cross-functional collaboration with planning and grooming issues. Need: clear prioritization
 
### Meetings
* Doesn't include recurring company/stage group meetings*
 
* UX buddy syncs
* Sync with Fabien
* Discuss maturity level sync
* Coffee chat with Seth Berger
* Sync with Mo
 
### 12.4 Review
[View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)