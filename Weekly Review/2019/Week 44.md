# October 28th to November 1st

Was OOO sick mostly from 30th-1st, moved most focus items to week 45

## Focus
* Review and groom 12.5 needs
* Review 12.6 issues

#### Progress
..


#### Opportunities
..
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* Sync w/ Adam C.: team intro and discuss https://gitlab.com/gitlab-org/gitlab/issues/9384
* UX buddy syncs w/ Camellia
* Onboarding w/ Annabel
* Design review w/ Phillippe
* Discovery collab w/ Fabien
* Pair design w/ Dimitrie
* Coffee chat
 
### Milestone Review

* [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing)

* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)


