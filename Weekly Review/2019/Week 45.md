# November 4th - 8th
Low bandwidth - Still ameliorating from being OOO sick 

## Focus
* Catch up from being OOO sick: ToDos and issues
* Ideate on auto-remediation MVCs, leverging new findings page
* Review and starting planning for %12.6


#### Progress
* Design iteration II, for auto-remediation: https://gitlab.com/gitlab-org/gitlab/issues/14059#note_240956001
* Proposed plan for auto-remediation path forward (including solution validation research): https://gitlab.com/gitlab-org/ux-research/issues/348#note_241388277


#### Opportunities
..
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* UX buddy syncs w/ Camellia
* Onboarding w/ Annabel
* Design review w/ Olivier
* Discovery collab w/ Fabien
* Pair design w/ Dimitrie
 
### Milestone Review

* [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing)

* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)
