# November 11th - 15th
OOO on Monday for Veterans Day Holiday

## Focus
* Finalize %12.5 issues, and MR UX reviews
* Ideation on MVC for https://gitlab.com/gitlab-org/gitlab/issues/34773
* Brainstorm 12.6 research efforts: combine study to include security/license gates and auto-remediation MVC
* Finalize auto-remediation discovery: https://gitlab.com/gitlab-org/gitlab/issues/14059#note_240956001. ToDos: BE/FE feedback on proposal, update per revisions needed, create resulting issues
* Groom %12.6 issues


#### Progress
* Auto-remediation discovery conclusion update: https://gitlab.com/gitlab-org/gitlab/issues/14059#note_245374632
* Sparklines merged - Included in GitLab UI design library: https://gitlab.com/gitlab-org/gitlab/merge_requests/19745
* User awareness when security scans are not configured - design ideation: https://gitlab.com/gitlab-org/gitlab/issues/34773#note_246015867

#### Opportunities
..
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* UX buddy syncs w/Camellia
* Onboarding weekly w/Annabel
* Sync w/Andy
* Sync w/Valerie
* Design review w/ Phillipe
* Discovery collab w/ Fabien
* Discovery conclusion collab w/Tetiana and Sam
* Research planning w/ Tali
* Pair design w/ Dimitrie
 
### Milestone Review

* [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing)

* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)


