# November 25th - 29th
OOO on Thursday for US Holiday, Friday is PTO

## Focus
* Create prototype for: https://gitlab.com/gitlab-org/ux-research/issues/206 and https://gitlab.com/gitlab-org/ux-research/issues/530 (look into Figma for prototype creation)
* Review epic status and consider closing, as most issues have been or will be shipped: https://gitlab.com/groups/gitlab-org/-/epics/1618

#### Progress
* Built prototype shell for user testing 
* Worked on test script flow, which will look at: auto-remediation MVC, vulns object page, and UX in MR w/ security gates active

#### Opportunities
..
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* TBD
 
### Milestone Review

* 12.6 *coming soon*

* [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing)

* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)

