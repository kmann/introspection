# September 9-13
Will be out on PTO starting on Thursday the 12th until Thursday 19th
## Focus
* Planning and grooming for 12.4 
* Prep for OOO / clear out ToDos
* Work on MR flow: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
* Licence compliance: what is “minimal”? Team alignment needed. 
 
#### Progress
* User testing plan for https://gitlab.com/gitlab-org/gitlab-ee/issues/13646 (related https://gitlab.com/gitlab-org/gitlab-ee/issues/13638) and https://gitlab.com/gitlab-org/gitlab-ee/issues/13582; will research both https://gitlab.com/gitlab-org/ux-research/issues/359 and https://gitlab.com/gitlab-org/ux-research/issues/360 in one study. 

#### Opportunities
* Clarify license compliance roadmap and priorities. [WIP: writing up](https://docs.google.com/document/d/17trOygi56BgBC6T-rnx5MkAA0LF5R3GVR4Cjr5OipBA/edit?usp=sharing) our current feature set, what's in progress, and future possiblities (for prioritization feedback). Aimed at identifying what is "minimal" and "viable" and "lovable".  
 
### Meetings
*Doesn't include recurring company/stage group meetings*
* 1 coffee chats
* UX buddy syncs
* Discussion w/ Mo about compliance issue: https://gitlab.com/gitlab-org/gitlab-ee/issues/12941 (introducing policy), license "obligations" concept, and mini book club
