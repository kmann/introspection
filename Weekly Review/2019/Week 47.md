# November 18th - 22nd


## Focus
* Review %12.5 issues - take a look a %12.6 (re-work per all the issues milestone changes)
* Plan, organize, and update 12.6 research efforts: study to include security/license gates and auto-remediation MVC (if time available add licence compliance classifications). AR: https://gitlab.com/gitlab-org/ux-research/issues/530, gates: https://gitlab.com/gitlab-org/ux-research/issues/206, LC classification: TBD
* Write up needed issues for auto-remediation discovery conclusion: https://gitlab.com/gitlab-org/gitlab/issues/14059#note_240956001.
* Update issue per feedback from design team and Phillipe: https://gitlab.com/gitlab-org/gitlab/issues/34773
* Prep for UX presentations: Secure UX weekly review (LC classifications and gate location) and UX showcase (configuration efforts)
* Take a looks at workflow labels - identify what the correct label would be to replace UX Ready. Example: when an implementation issue is ready and scope defined, does it remain workflow::design until engineering grooms it?


#### Progress
* Cross-stage research collaboration: https://gitlab.com/gitlab-org/ux-research/issues/270#note_246750382
* Organized joint validation studies looking at https://gitlab.com/gitlab-org/ux-research/issues/206 and https://gitlab.com/gitlab-org/ux-research/issues/530
* Outlined path forward for auto-remediation efforts: https://gitlab.com/groups/gitlab-org/-/epics/2238

#### Opportunities
..
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* UX buddy syncs w/Camellia
* Onboarding weekly w/Annabel
* Design review w/ Olivier
* Discovery collab w/ Fabien
* Sync w/Andy
* Sync w/Valerie
* Pair design w/ Dimitrie
* UX showcase: configuration efforts
 
### Milestone Review

* 12.6 *coming soon*

* [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing)

* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)


