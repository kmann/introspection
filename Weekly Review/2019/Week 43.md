# October 21th-25th
OOO on Friday 25th
## Focus
* Review, groom, edit, and add issues for 12.5 needs
* Research and ideate on auto-remediation MVCs
* Review and prioritize research efforts: security/license gates

#### Progress

* Closed configure from UI discovery: https://gitlab.com/gitlab-org/gitlab/issues/13646#discovery-conclusion
* Updated description for Secure UI configuration screen and marked `UX Ready`: https://gitlab.com/gitlab-org/gitlab/issues/13638
* Updated description for https://gitlab.com/gitlab-org/gitlab/issues/14061. Exciting issue since this bootstraps policy for LC. It was scaled back a bit, and policy editing/adding will be added in %12.6 https://gitlab.com/gitlab-org/gitlab/issues/33870



#### Opportunities
* Did a discovery retro with Mo and Fernando for https://gitlab.com/gitlab-org/gitlab/issues/12941#discovery-outcomes, here are [the notes](https://docs.google.com/document/d/19_3P8N3bPOemVsn1EJS4vsuAQmh4NKnqDjmqHgUxACA/edit?usp=sharing)
* Secure retro notes: https://gitlab.com/gl-retrospectives/secure/issues/8#note_232469979
* Discovery feedback: https://gitlab.com/gitlab-org/gitlab/issues/34886#note_235151769
 
### Meetings
*Doesn't include recurring company/stage group meetings*
 
* Connect with Rebecca Dodd about blog bio/profile concept
* UX buddy syncs
* Sync with Fabien about new configuration discovery
* Coffee chat
 
### 12.4 Review
* [View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)