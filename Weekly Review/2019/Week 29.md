## Focus
* Follow up on dashboard recommendations https://gitlab.com/gitlab-org/gitlab-design/issues/460 : https://gitlab.com/gitlab-org/gitlab-ee/issues/12846 and https://gitlab.com/gitlab-org/gitlab-ee/issues/12881
* License compliance epic: https://gitlab.com/groups/gitlab-org/-/epics/1618
* https://gitlab.com/gitlab-org/gitlab-ee/issues/6924
* Talk to stakeholders and identify path forward, discovery doc
* Customer interview

### Meetings
*Doesn't include recurring company/stage group meetings*
* Review license management with Nicole  (30 min)
* Prep with Tali for customer interviews (30 min)
* Customer interview I
* Customer interview II
* Sync with Fabien to review discovery results

