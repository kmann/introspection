# September 2-6
US holiday on Monday - Labor day
## Focus
* Welcome Camellia to the team!
* Planning and grooming for 12.4 
* Finalize any 12.3 needs
* Drive conversation about https://gitlab.com/gitlab-org/gitlab-ee/issues/12937, prepping for https://gitlab.com/gitlab-org/gitlab-ee/issues/12941
* Work with Fabien on: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646 and https://gitlab.com/gitlab-org/gitlab-ee/issues/13638
 
#### Progress
* UX Ready, License list: https://gitlab.com/gitlab-org/gitlab-ee/issues/13582
* Clarifying messaging to the user (LC): https://gitlab.com/gitlab-org/gitlab-ee/issues/12190#note_212235674
* Proactively inform the user: https://gitlab.com/gitlab-org/gitlab-ee/issues/12190#note_213868781
* Introducing licence classification names:  https://gitlab.com/gitlab-org/gitlab-ee/issues/12937#note_211525928

 
#### Opportunities
* Team alignment on the definition of “minimal” for license compliance feature
 
### Meetings
*Doesn't include recurring company/stage group meetings*
* 2 coffee chats
* LC demo and discussion w/ Mo
* Sync with Fabien and Tetiana about: https://gitlab.com/gitlab-org/gitlab-ee/issues/12190
* Brainstorm with Fabien about: https://gitlab.com/gitlab-org/gitlab-ee/issues/11119
 

