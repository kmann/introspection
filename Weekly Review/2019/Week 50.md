# December 9th - 13th

## Focus
* Review user study results and ideate on solution concepts
* Work on project level dashboard layout enhancement (per user interview feedback)
* Continue Secure/Defend settings discussion review
* Prepare walkthrough document and schedule live sync for new product members
* Review 12.7/8 for planning review and recommendations
* Work on any missed ToDos [View %12.5 status document](https://docs.google.com/document/d/1uMN_ck1F7TdRKJHvkV4EaAhsy9IK5D7YVGS0om5hLvw/edit?usp=sharing), migrate slipped items to 12.6/7 document as needed

#### Progress
* License compliance UX foundation epic: https://gitlab.com/groups/gitlab-org/-/epics/2328
* Cross-team discussion about info-architecture and recents findings UX https://gitlab.com/gitlab-org/gitlab/issues/39209/

#### Opportunities
* Improving onboarding for new Secure team members: informing about current baseline UI/interaction and ongoing UX strategy
* Prioritize our efforts and align on strategy about security approver rules - the recent scorecard is a great kickoff to this initiative
* Improve project vulnerability list: https://gitlab.com/gitlab-org/gitlab/issues/39091
 
### Meetings
*Doesn't include recurring company/stage group meetings*

* User test results analysis review w/ Tali and Camellia
* UX buddy weekly w/ Camellia
* Pair design review w/ Dimitrie
* Review w/ Phillipe
* Coffee chat w/ Andrew A. 
 
### Milestone Review

* 12.6/7 *coming soon*
