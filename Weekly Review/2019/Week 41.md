# October 7th-11th
## Focus
* Review https://gitlab.com/gitlab-org/gitlab/issues/14059 - try to identify key questions to consider. 
* Review https://gitlab.com/gitlab-org/ux-research/issues/360 and https://gitlab.com/gitlab-org/ux-research/issues/359. Identify key user feedback and incorporate into https://gitlab.com/gitlab-org/gitlab/issues/13638 and https://gitlab.com/gitlab-org/gitlab/issues/13646
* Review, groom, and identify 12.5 needs
#### Progress
* Issue kickoff with Camellia on a first issue: https://gitlab.com/gitlab-org/gitlab/issues/13992
* Identified a cross-functional plan for implementation on: https://gitlab.com/gitlab-org/gitlab/issues/12941
 
#### Opportunities
* Discovery kickoff process, will get feedback from colleauges and consider MR for sugguestions for improvement
* Collaborate across the team on long term goals that may be merged together 
 
### Meetings
*Doesn't include recurring company/stage group meetings*
 
* UX buddy syncs: design system review, issue review, issue kickoff, weekly 1:1
* Sync w/Amelia about design system and GitLab UI
* Setup discovery review
* Sync with Fabien about: `https://gitlab.com/gitlab-org/gitlab/issues/13646`
 
### 12.4 Review
[View %12.4 status document](https://docs.google.com/document/d/1dZw5rsoisY2sQjw4nTHgFKcLCB9OF3oHAZhmnNHq_JI/edit?usp=sharing)