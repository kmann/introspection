## July 8-12
* Out of office Friday the 12th

## Planned Focus

* Review 12.1 issues
* [ ] Finalize any outstanding needs
* [ ] Reach out to FE and BE to check in if any UX concerns

* Review upcoming 12.2 issues
* [ ] Estimate scheduling and grooming
* [ ] Identify issue prep needs

* Create issues from license management and sec dashboard baseline audit
* [ ] TBD

* Start reviewing and researching:  https://gitlab.com/gitlab-org/gitlab-ee/issues/6924
* [ ] Identify next steps
* [ ] Create issue kickoff template

* Follow up on issues and work on wireframe
* [] https://gitlab.com/gitlab-org/gitlab-ee/issues/12485#note_188869182

### Unplanned Focus

* TBD

### Meetings
*Doesn't include recurring company/stage group meetings*

* [ ] 12.2 Secure UX Issue Review



