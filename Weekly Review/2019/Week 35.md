# August 26-30
Will meet and co-work with Pedro onsite on Thursday 29th : )
## Focus
* Prioritize Day I issue, prep for 12.4: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638
* Work through iteration 3 for improved Secure configuration, based on last feedback session: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
* License compliance epic path forward: https://gitlab.com/groups/gitlab-org/-/epics/1618
* Cross stage group research effort: approver feature and security gates rule https://gitlab.com/gitlab-org/gitlab-design/issues/529#note_201798708
* Create new JTBD issue: 1) configuration (individual test and/or Auto), 2) dependency list
* Continue working on [draft](https://docs.google.com/document/d/11ZGIiOPTqSZj4kcrBcQ2c9pFPq1VthjcSdB6l77KpnI/edit?usp=sharing) for upcoming Secure UX team blog post about collaboration, scaling, and evolving our group.


#### Progress
* Updated epic: https://gitlab.com/groups/gitlab-org/-/epics/1618 with notes, and added as sub epic to https://gitlab.com/groups/gitlab-org/-/epics/273 (which is issues that need to ship in 3 or less milestones)
* Prioritized issue for 12.4: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638
     * Focuses on improving user awareness of Secure features and guiding them to the right documentation to configure
* Discovery issues for 12.4: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
     * Focuses on user being able to activate a Secure feature in the UI (new configuration section above)
* Ability to add optional note to license classification: https://gitlab.com/gitlab-org/gitlab-ee/issues/10534#note_208540926
* Adding license compliance to group level: https://gitlab.com/gitlab-org/gitlab-ee/issues/7149#note_206679825
* Clarifying license results in MR: https://gitlab.com/gitlab-org/gitlab-ee/issues/9329#note_206683512
* Completed team [blog](https://docs.google.com/document/d/11ZGIiOPTqSZj4kcrBcQ2c9pFPq1VthjcSdB6l77KpnI/edit?usp=sharing) for [MR](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5178)


#### Opportunities
* Improve how we communicate in UI about configuration status
     * Focuses on improving UX of Secure pages, when features have not been configured: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
     * This issue about customers that don’t have Ultimate, but are trying to add the feature (but don’t realize they need Ultimate): https://gitlab.com/gitlab-org/gitlab-ee/issues/13823

### Meetings
*Doesn't include recurring company/stage group meetings*
* 2 coffee chats
* Security gates research prep, w/ Tali
* Baseline initiatives for Defend discussion, w/ Becca 
* Product vision meeting

<details> <summary>12.3 Issue status</summary>
### 12.3 Issue status


##### Not started
* Show warning when the Dependency List is not up-to-date: https://gitlab.com/gitlab-org/gitlab-ee/issues/12190 (may be changed to 12.4)
* Create, Build, and Style > Broadcast messages: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/298/designs

##### UX Ready
* Clarify detected license results in MR: https://gitlab.com/gitlab-org/gitlab-ee/issues/12530

* License Compliance Approvals in Merge Request MVC: https://gitlab.com/gitlab-org/gitlab-ee/issues/13067


##### Unplanned
* Improve day 1 UX for Secure features: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638

* Add configuration section to Security and Compliance: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646

* Evaluate adding batch dismiss option to security dashboards: https://gitlab.com/gitlab-org/gitlab-ee/issues/11425

* Evaluate adding batch dismiss option to security dashboards: https://gitlab.com/gitlab-org/gitlab-ee/issues/11425

* Display dependencies a license is being used in: https://gitlab.com/gitlab-org/gitlab-ee/issues/12940

* License management settings: user awareness if setup configuration is not complete: https://gitlab.com/gitlab-org/gitlab-ee/issues/12685

* Auditing licenses already merged into a project: https://gitlab.com/gitlab-org/gitlab-ee/issues/12938

* Introduce automation to license management: https://gitlab.com/gitlab-org/gitlab-ee/issues/12941


##### Research - need to prep for %12.4
* Think-aloud test for Security Dashboard changes: https://gitlab.com/gitlab-org/ux-research/issues/240

* Usability and product validation of security approval MVC: https://gitlab.com/gitlab-org/ux-research/issues/206
 
</details>
 