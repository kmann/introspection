# September 23-27


## Focus
* Catch up on ToDos from being OOO
* Update prototype and prep for testing: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646 (related https://gitlab.com/gitlab-org/gitlab-ee/issues/13638) and https://gitlab.com/gitlab-org/gitlab-ee/issues/13582; will research both https://gitlab.com/gitlab-org/ux-research/issues/359 and https://gitlab.com/gitlab-org/ux-research/issues/360 
* Licence compliance: what is “minimal”?
* Work on https://gitlab.com/gitlab-org/gitlab-ee/issues/12941 (introducing policy)
 
#### Progress
* Insightful and actionable feedback from user testing 
* [Established foundations](https://docs.google.com/document/d/1VHhT75dHpP4eYBiMxrDtNkSfQdwpsnugT0wa0Tqm9wk/edit?usp=sharing) for [lc policy discovery](https://gitlab.com/gitlab-org/gitlab/issues/12941) • [UX review posted to GitLab Unfiltered](https://youtu.be/dli8e2aQyEI)


#### Opportunities
* Review and take action on user test feedback 
* Bring visiblity to the wider team about long term product/ux goals

### Meetings
*Doesn't include recurring company/stage group meetings*
* 2 coffee chats
* UX buddy syncs
* Skip level UX directing meeting
* Sync with Marin ([video](https://drive.google.com/file/d/1xUUQ8bb4prOEjv90SUAAT0DYa-FPXdjx/view?usp=sharing)) 
* 5 user test sessions