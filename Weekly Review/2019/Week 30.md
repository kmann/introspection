# July 22-26
Low-bandwidth: part-time sick day on Monday/Tuesday. 
## Focus
* Organize next steps license compliance epic and issues: https://gitlab.com/groups/gitlab-org/-/epics/1618
* Information design epic review/organize: https://gitlab.com/groups/gitlab-org/-/epics/1591
* Iron out requirements and start designs: https://gitlab.com/gitlab-org/gitlab-ee/issues/10536
* Clarify next steps to get UX Ready: https://gitlab.com/gitlab-org/gitlab-ee/issues/6924

#### Progress
* Design review with Amelia https://gitlab.com/gitlab-org/gitlab-ee/issues/12881#note_194458063 
* Implementation with Sam B. and adding to GitLabUI https://gitlab.com/gitlab-org/gitlab-ui/issues/366
* Future need: create MR with sparkline notes and recommended usage in Pajamas
* Marked UX ready: https://gitlab.com/gitlab-org/gitlab-ee/issues/10536#note_195367121
* Iteration w/ Jeremy: https://gitlab.com/gitlab-org/gitlab-ee/issues/12530

### Meetings
*Doesn't include recurring company/stage group meetings*
* Security Gate: BE implementation
* TBD: reach out to our security/compliance team (LC discovery) 


### 12.2 Issue status

* Engineering discovery for first-class vulnerabilities
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: not started
* Engineering discovery for first-class vulnerabilities
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: not started
* UX product discovery for first-class vulnerabilities 
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
    * Status: not started - details unknown
* Secure experience baselines: managing licenses (accountability)
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: complete
* Add License information to the Dependency List based on current license rules
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10536
     * Status: on track - gathering requirements
* Create, Build, and Style > Banners
     * https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/300
     * Status: -
* Engineering & UX Discovery: Disallow merge if a blacklisted license is founds
    * https://gitlab.com/gitlab-org/gitlab-ee/issues/6924
     * Status: Ongoing, next steps pending decisions: 
* Secure experience baselines: managing licenses (responsibility) 
     * https://gitlab.com/gitlab-org/gitlab-design/issues/478
     * Status: Complete
* Update Security Dashboard layout for improved usability
     * https://gitlab.com/gitlab-org/gitlab-design/issues/478
     * Status: On track, some corrections are needed on the line graph to align with our design library.
* Show most affected projects in Group Security Dashboard
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/11190
     * Status: On track, some corrections are needed on the line graph to align with our design library.
* Show on dashboard when security tests are not run
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/7521
     * Status: Off track - need to understand and identify data we are working with



