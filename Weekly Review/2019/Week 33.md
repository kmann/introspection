# August 12-16
Friday the 16th meeting with Fabien in person; focusing on License compliance 
## Focus
* Organize, research, and review 12.3 issues
* Sparkline documentation to Pajamas - prep for upcoming: https://gitlab.com/gitlab-org/gitlab-ui/issues/366
* Brainstorm on license compliance 
     * Audit and review existing issues
     * Partner with GitLab internal team
     * Identify user type (legal?)
* Dashboard layout for responsive design: https://gitlab.com/gitlab-org/gitlab-ee/issues/13419
* Updated dashboard validation/think-aloud feedback issue: https://gitlab.com/gitlab-org/ux-research/issues/240

#### Progress
* Great feedback, ideation, and action items from group design review (early ideation of day I and set up of secure features UX): https://youtu.be/zLLeNAEeU68
* Internal interview with head of compliance - uncovered our current process and policies we are working on. Will update at next Secure UX meeting.
* Issues to bootstrap license compliance feature: https://gitlab.com/gitlab-org/gitlab-ee/issues/13582 and https://gitlab.com/gitlab-org/gitlab-ee/issues/12941
   
#### Opportunities
* Partner with Andy on collaboration and team scaling retrospective
* Partner with Secure UX team on Day I and feature setup UX issue: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638. Mentioned to Becca could be a great shadow and/or shared effort issues for onboarding. 

### Meetings
*Doesn't include recurring company/stage group meetings*
* 3 coffee chats: 
* Sync with Candice about compliance
* Sonatype webinar 


## 12.3 Overview


##### Not started
* Show warning when the Dependency List is not up-to-date
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/12190
* Create, Build, and Style > Broadcast messages
     * https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/298/designs

##### UX Ready
* Clarify detected license results in MR
   * https://gitlab.com/gitlab-org/gitlab-ee/issues/12530
* License Compliance Approvals in Merge Request MVC
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/13067

##### Unplanned
* Evaluate adding batch dismiss option to security dashboards
   * https://gitlab.com/gitlab-org/gitlab-ee/issues/11425
* Display dependencies a license is being used in
   * https://gitlab.com/gitlab-org/gitlab-ee/issues/12940
* License management settings: user awareness if setup configuration is not complete
   * https://gitlab.com/gitlab-org/gitlab-ee/issues/12685
* Auditing licenses already merged into a project
   * https://gitlab.com/gitlab-org/gitlab-ee/issues/12938
* Introduce automation to license management
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/12941
 * Security dashboard: responsive layout
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/13419

##### Research - need to prep for %12.4
* Think-aloud test for Security Dashboard changes 
     * https://gitlab.com/gitlab-org/ux-research/issues/240
* Usability and product validation of security approval MVC
     * https://gitlab.com/gitlab-org/ux-research/issues/206