# August 5-9

### Focus
* Intro discussion with Pedro about Secure partnering with Create on approvers feature vision and next steps.
* Work on baseline initiative validation plans https://gitlab.com/gitlab-org/gitlab-design/issues/528
     * https://gitlab.com/gitlab-org/gitlab-design/issues/510 and https://gitlab.com/gitlab-org/gitlab-design/issues/511
     * https://gitlab.com/gitlab-org/gitlab-design/issues/509
     * Create new baseline issue for `Vulnerability-Check`
* Finalize 12.3 grooming
     * Update UX grooming document
     * Clear out any needs from 12.2
     * Create implementation issues for 12.3 (https://gitlab.com/gitlab-org/gitlab-ee/issues/7521) 
     * Mark relevant issues `UX Ready`
* Prep for compliance meeting: learning more about internal compliance processes

#### Progress
*  %12.4 implmentations https://gitlab.com/gitlab-org/gitlab-ee/issues/12881 and https://gitlab.com/gitlab-org/gitlab-ee/issues/13298


#### Opportunities
* Partnering with Pedro and Marcel from Create to improve approvals feature. Next steps, we plan on doing research (https://gitlab.com/gitlab-org/gitlab-design/issues/529#note_201798708), then we can follow up with each other on findings to plan design improvements.
* Collaborate with Andy on upcoming baseline initiative for `Vulnerability-Check`: recommendations influence both in-stage groups. https://gitlab.com/gitlab-org/gitlab-design/issues/533 and https://gitlab.com/gitlab-org/gitlab-design/issues/534 (related: https://gitlab.com/gitlab-org/gitlab-design/issues/479)
* Leveraging our [Secure UX](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/secure/) page to easily share baseline progress, recommendations, and current research. Add links to group discussions for visibility and to encourage more feedback for improvements.
* Learn more about dependency list use cases (another baseline?)

### Meetings
*Doesn't include recurring company/stage group meetings*
* 2 coffee chats
* Research review with Tali about SAST setup
* Process review with Candice Ciresi (Head of Compliance at GitLab)

### 12.2 Issue status

* Engineering discovery for first-class vulnerabilities
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: not started
* UX product discovery for first-class vulnerabilities 
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
    * Status: not started - details unknown
* Secure experience baselines: managing licenses (accountability)
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: Complete
* Add License information to the Dependency List based on current license rules
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10536
     * Status: UX Ready
* Create, Build, and Style > Banners
     * https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/300
     * Status: -
* Engineering & UX Discovery: Disallow merge if a blacklisted license is founds
    * https://gitlab.com/gitlab-org/gitlab-ee/issues/6924
     * Status: Closed and opened https://gitlab.com/gitlab-org/gitlab-ee/issues/13067 (UX Ready)
* Secure experience baselines: managing licenses (responsibility) 
     * https://gitlab.com/gitlab-org/gitlab-design/issues/478
     * Status: Complete
* Update Security Dashboard layout for improved usability
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/12846
     * Status: UX Ready
* Show most affected projects in Group Security Dashboard
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/11190
     * Status: UX Ready
* Discovery: Show on dashboard when security tests are not run
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/7521
     * Status: closed https://gitlab.com/gitlab-org/gitlab-ee/issues/7521#note_195264318 and opened https://gitlab.com/gitlab-org/gitlab-ee/issues/13298 (UX Ready)