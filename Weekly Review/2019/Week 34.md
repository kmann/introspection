# August 19-23

## Focus
* Day I and setup issues: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638 and https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
* License compliance epic path forward: https://gitlab.com/groups/gitlab-org/-/epics/1618
* Cross stage group research effort: approver feature and security gates rule https://gitlab.com/gitlab-org/gitlab-design/issues/529#note_201798708
* Consider new JTBD: 1) configuration (individual test and/or Auto), 2) dependency list
* Feedback from dogfooding discussion with compliance

#### Progress
* Path forward established for day and configuration improvement issues:
     * %12.4 prioritized for implementation: https://gitlab.com/gitlab-org/gitlab-ee/issues/13638
     * %12.4 product/engineering discovery planned: https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
* [Zero draft created](https://docs.google.com/document/d/11ZGIiOPTqSZj4kcrBcQ2c9pFPq1VthjcSdB6l77KpnI/edit?usp=sharing) for upcoming Secure UX team blog post about collaboration, scaling, and evolving our group


#### Opportunities
* New JBTDs for discussion with product
* Need customer research around license compliance



### Meetings
Doesn't include recurring company/stage group meetings
* 3 coffee chats
* SAST setup research results review
* Sync w/ Tali about security gates testing and specify/consider JBTD for setting up Secure features - add to [Secure UX](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/secure/)
* Sync w/ Becca about https://gitlab.com/gitlab-org/gitlab-ee/issues/13638 and https://gitlab.com/gitlab-org/gitlab-ee/issues/13646
* Synced with Fabien and Tali about configuration concepts
* Stage group retro

### 12.3 Issue status


##### Not started
* Show warning when the Dependency List is not up-to-date: https://gitlab.com/gitlab-org/gitlab-ee/issues/12190
* Create, Build, and Style > Broadcast messages: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/298/designs

##### UX Ready
* Clarify detected license results in MR: https://gitlab.com/gitlab-org/gitlab-ee/issues/12530

* License Compliance Approvals in Merge Request MVC: https://gitlab.com/gitlab-org/gitlab-ee/issues/13067

##### Unplanned
* Evaluate adding batch dismiss option to security dashboards: https://gitlab.com/gitlab-org/gitlab-ee/issues/11425

* Display dependencies a license is being used in: https://gitlab.com/gitlab-org/gitlab-ee/issues/12940

* License management settings: user awareness if setup configuration is not complete: https://gitlab.com/gitlab-org/gitlab-ee/issues/12685

* Auditing licenses already merged into a project: https://gitlab.com/gitlab-org/gitlab-ee/issues/12938

* Introduce automation to license management: https://gitlab.com/gitlab-org/gitlab-ee/issues/12941


##### Research - need to prep for %12.4
* Think-aloud test for Security Dashboard changes: https://gitlab.com/gitlab-org/ux-research/issues/240

* Usability and product validation of security approval MVC: ttps://gitlab.com/gitlab-org/ux-research/issues/206