# July 29-2
Will be attending the security training courses on Monday and Tuesday
## Focus
* Create implementation issue based on https://gitlab.com/gitlab-org/gitlab-ee/issues/6924 (https://gitlab.com/gitlab-org/gitlab-ee/issues/13067)
* Identity design needed for issues: https://gitlab.com/groups/gitlab-org/-/epics/1618
* Finalize issues that are not UX ready and/or outstanding questions-answers needed
* Reach out to our internal compliance team to understand their blacklisting and/or approvals process 
* Reach out to sales team to try to find active Secure customers

#### Progress
* Identified an area of automation: https://gitlab.com/gitlab-org/gitlab-ee/issues/12941#note_196726639
* Partnered with Andy to align https://gitlab.com/gitlab-org/gitlab-ee/issues/12846, https://gitlab.com/gitlab-org/gitlab-ee/issues/11190, and https://gitlab.com/gitlab-org/gitlab-ee/issues/7521
* Partnered with Avielle to identify data points and produced solution proposal for https://gitlab.com/gitlab-org/gitlab-ee/issues/7521


#### Opportunities
* Issues include cross-functional assignees. For example, if it involves BE, FE, and UX there should be a representative from each. 
     * Ideally, include this in planning
     * Great for team alignment and shared awareness  
* Baseline auditing: work with other stages to exchange heuristic reviews
* Partner with create on approval feature vision
* Dogfooding `Vulnerability-Check` first steps: https://gitlab.com/gitlab-org/gitlab-ce/issues/65392#note_199166290. Other for consideration is `License-Check`
* Similar, create baseline JTBD for above approval checks

### Meetings
*Doesn't include recurring company/stage group meetings*
* 3 coffee chats
* Design review with Marcel, #ux-coworking
* RoR trainings 

### 12.2 Issue status

* Engineering discovery for first-class vulnerabilities
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: not started
* UX product discovery for first-class vulnerabilities 
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
    * Status: not started - details unknown
* Secure experience baselines: managing licenses (accountability)
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10252
     * Status: Complete
* Add License information to the Dependency List based on current license rules
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/10536
     * Status: UX Ready
* Create, Build, and Style > Banners
     * https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/300
     * Status: -
* Engineering & UX Discovery: Disallow merge if a blacklisted license is founds
    * https://gitlab.com/gitlab-org/gitlab-ee/issues/6924
     * Status: Complete, updated implementation issues this week (https://gitlab.com/gitlab-org/gitlab-ee/issues/13067), will mark UX Ready next week
* Secure experience baselines: managing licenses (responsibility) 
     * https://gitlab.com/gitlab-org/gitlab-design/issues/478
     * Status: Complete
* Update Security Dashboard layout for improved usability
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/12846
     * Status: UX Ready
* Show most affected projects in Group Security Dashboard
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/11190
     * Status: UX Ready
* Discovery: Show on dashboard when security tests are not run
     * https://gitlab.com/gitlab-org/gitlab-ee/issues/7521
     * Status: In progress, updated proposal https://gitlab.com/gitlab-org/gitlab-ee/issues/7521
