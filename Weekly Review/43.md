# October 19th - October 23th
...

## Focus
* Schedule scan policy MVC
* ~~Start policy creation and alert management UXR findings analysis/review~~ (If time available, priorities have changed due to Protect)
* MR review for upcoming release
* Aim to close out open button/dropdown issues (per pipeline errors or styling feedback needed)
* [Ongoing](https://docs.google.com/document/d/17o8FyReQOYGBJYPcz1HhJZvFyYFwQYtCGzAfrE435Yw/edit?usp=sharing)

##### ToDos 
* [X] Update JTBD ahead of workshop

#### Progress
* Updates to [prototype](https://gitlab-org-threat-management-defend-demos-policy-mock.34.83.185.53.nip.io/create.html#): navigation changes, new policies section updates, schedule scan creation flow (modal for creating profile "Saved scan")
* Continueing to resolve two tricky/lingering migration MRs: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42325 and https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42075


### Meetings
*Doesn't include recurring company/stage group meetings*

* Sam W PM/UX sync
* 3 mentor/mentee chats
* Catch up w/ Dimitrie
* Catch up w/ Marcel
* HR and manager call
* Sync w/ Mo 




